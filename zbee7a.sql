-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 01, 2019 at 07:33 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zbee7a`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

DROP TABLE IF EXISTS `about_us`;
CREATE TABLE IF NOT EXISTS `about_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_ar` varchar(225) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `title_en` varchar(225) NOT NULL DEFAULT '',
  `details_ar` text CHARACTER SET utf8 NOT NULL,
  `details_en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`) VALUES
(1, 'عنوان عنوان', 'title title', 'يعتبر تطبيق الذبائح من اسهل الطرق لشراء الذبائح ا،ن لاين و بكل سهوله و سلاسه', 'Elmasry Gym one of the best Gyms at Egypt\r\nWe have amazing trainers \r\nWe sell everything');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(225) NOT NULL,
  `details_en` text CHARACTER SET utf8 NOT NULL,
  `details_ar` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `link` varchar(225) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `image`, `details_en`, `details_ar`, `link`, `type`) VALUES
(1, 'IMG-20190411-WA0008.jpg', 'عندنا خصم ١٠ في الميه علي ', 'عندنا خصم ١٠ في الميه علي ', 'http://www.tripadvisor.com', 0),
(2, 'IMG-20190411-WA0009.jpg', 'عندنا خصم ٨ في الميه علي ', 'عندنا خصم ٨ في الميه علي ', 'https://soundcloud.com/', 0),
(3, 'b2r.jpg', 'عندنا خصم ١٥ في الميه علي ', 'عندنا خصم ١٥ في الميه علي ', 'https://www.yelp.com', 0),
(4, 'n3emy.jpg', 'عندنا خصم ٥ في الميه علي ', 'عندنا خصم ٥ في الميه علي ', 'https://www.amazon.com', 0),
(5, 'nagdy.jpg', 'عندنا خصم ٥٠ في الميه علي ', 'عندنا خصم ٥٠ في الميه علي ', 'http://www.orange.com', 0),
(6, '7ashy.jpg', 'اطلب اي اوردر بخصم ٢٠ ٪', 'اطلب اي اوردر بخصم ٢٠ ٪', 'https://www.facebook.com/Khiratstore/photos/a.1929841700670977.1073741828.1923059071349240/2073805016274644/?type=3&theater', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
CREATE TABLE IF NOT EXISTS `cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_text_ar` varchar(225) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `case_text_en` varchar(225) NOT NULL,
  `color` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cases`
--

INSERT INTO `cases` (`id`, `case_text_ar`, `case_text_en`, `color`) VALUES
(1, 'تحت التجهيز', 'case1', 'ffffff');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

DROP TABLE IF EXISTS `codes`;
CREATE TABLE IF NOT EXISTS `codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `code` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favourite`
--

DROP TABLE IF EXISTS `favourite`;
CREATE TABLE IF NOT EXISTS `favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourite`
--

INSERT INTO `favourite` (`id`, `item_id`, `user_id`) VALUES
(139, 83, 7),
(140, 84, 7),
(141, 83, 7),
(142, 84, 7);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_ar` varchar(225) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL DEFAULT '',
  `title_en` varchar(225) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL DEFAULT '',
  `details_ar` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `details_en` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `price_before` double NOT NULL DEFAULT '0',
  `price_after` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `price_before`, `price_after`, `created_at`, `updated_at`) VALUES
(83, 'تيس عارضي', 'تيس عارضي', 'نحن نمتلك افضل و اجمل الذبائح', 'نحن نمتلك افضل و اجمل الذبائح', 5000, 4900, '2019-05-28 10:43:34', '0000-00-00 00:00:00'),
(84, 'نعيمي', 'نعيمي', 'نحن نمتلك افضل و اجمل الذبائح', 'نحن نمتلك افضل و اجمل الذبائح', 6500, 6400, '2019-05-28 10:43:34', '0000-00-00 00:00:00'),
(85, 'نجدي', 'نجدي', 'نحن نمتلك افضل و اجمل الذبائح', 'نحن نمتلك افضل و اجمل الذبائح', 12000, 11500, '2019-05-28 10:43:34', '0000-00-00 00:00:00'),
(86, 'حري', 'حري', 'نحن نمتلك افضل و اجمل الذبائح ', 'نحن نمتلك افضل و اجمل الذبائح', 5500, 5300, '2019-05-28 10:43:34', '0000-00-00 00:00:00'),
(87, 'حاشي', 'حاشي', 'نحن نمتلك افضل و اجمل الذبائح', 'نحن نمتلك افضل و اجمل الذبائح', 15000, 14000, '2019-05-28 10:43:34', '0000-00-00 00:00:00'),
(90, 'ذبيحة', 'item', 'وصف', 'desc', 500, 450, '2019-05-28 09:19:25', '2019-05-28 09:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `items_images`
--

DROP TABLE IF EXISTS `items_images`;
CREATE TABLE IF NOT EXISTS `items_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items_images`
--

INSERT INTO `items_images` (`id`, `item_id`, `image`) VALUES
(1, 90, 'image1.png'),
(2, 90, 'image2.png'),
(7, 83, 'zbee7a_image_item_83.png'),
(8, 84, 'zbee7a_image_item_84.png'),
(9, 85, 'zbee7a_image_item_85.png'),
(10, 86, 'zbee7a_image_item_86.png'),
(11, 87, 'zbee7a_image_item_87.png');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` double NOT NULL DEFAULT '0',
  `lang` double NOT NULL DEFAULT '0',
  `name_ar` varchar(225) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name_en` varchar(225) NOT NULL DEFAULT '',
  `details_ar` varchar(225) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `details_en` varchar(225) NOT NULL DEFAULT '',
  `phone` varchar(225) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `lat`, `lang`, `name_ar`, `name_en`, `details_ar`, `details_en`, `phone`) VALUES
(1, 23.3434, 33.3434, 'فرع مدينتي', 'Mdenty Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '0112193453'),
(2, 23.3434, 33.3434, 'فرع الشروق', 'Elshrouck Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383'),
(3, 23.3434, 33.3434, 'فرع العبور', 'Elabour Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383'),
(4, 23.3434, 33.3434, 'فرع التجمع الخامس', 'Altgmoh Alkhames Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383'),
(5, 23.3434, 33.3434, 'فرع ٦ اكتوبر', '6 October Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383'),
(6, 23.3434, 33.3434, 'فرع الرحاب', 'Rehab Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383'),
(7, 23.3434, 33.3434, 'فرع جسر السويس', 'Gesr Alsuez Branch', 'من افضل الفروع يوجد لدينا كل شي طازج ونصلك في اسرع وقت', 'We have everthing at our branch and our delevery so fast', '01121805383');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `message` mediumtext NOT NULL,
  `phone_or_mail` varchar(255) NOT NULL DEFAULT 'لا يوجد',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `phone_or_mail`) VALUES
(1, 1, 'mohjamed@yahoo.com', 'لا يوجد');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` tinytext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `text`, `created_at`) VALUES
(1, 'نص نص', '2019-06-01 00:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('awaiting','completed') NOT NULL,
  `user_id` int(11) NOT NULL,
  `final_price` double NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `case_id` int(11) NOT NULL,
  `from` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` mediumtext NOT NULL,
  `lat` double NOT NULL,
  `lang` double NOT NULL,
  `basket_items` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `user_id`, `final_price`, `date`, `case_id`, `from`, `to`, `address`, `lat`, `lang`, `basket_items`, `created_at`, `updated_at`) VALUES
(1, 'completed', 2, 23.67, '2019-05-29 02:39:03', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'address address', 13.543534, 15.543534, '', '2019-05-29 01:22:01', '2019-05-29 00:39:03'),
(2, 'completed', 1, 18.5, '2019-05-29 02:51:44', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', '', 0, 0, '83,84', '2019-05-29 01:22:01', '2019-05-29 00:51:44'),
(3, 'awaiting', 1, 15100, '2019-05-29 02:13:00', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', '', 0, 0, '83,84,85', '2019-05-29 01:22:01', NULL),
(4, 'awaiting', 1, 15100, '2019-05-29 02:13:02', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'addo', 0, 0, '83,84,85', '2019-05-29 01:22:01', NULL),
(5, 'completed', 1, 0, '2019-05-29 02:51:51', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', '', 0, 0, '83,84,85', '2019-05-29 01:22:01', '2019-05-29 00:51:51'),
(6, 'awaiting', 1, 1450, '2019-05-29 02:13:05', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'iu', 0, 0, '83,84,85', '2019-05-29 01:22:01', NULL),
(7, 'awaiting', 6, 10550, '2019-05-29 02:13:07', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'address', 0, 0, '83,84,85', '2019-05-29 01:22:01', NULL),
(8, 'awaiting', 6, 0, '2019-05-28 09:43:47', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'hello', 0, 0, '86,87,88', '2019-05-29 01:22:01', NULL),
(9, 'awaiting', 1, 6400, '2019-05-28 09:43:47', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'zamalek', 0, 0, '84', '2019-05-29 01:22:01', NULL),
(10, 'awaiting', 1, 11500, '2019-05-28 09:43:47', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'zamalek', 0, 0, '85', '2019-05-29 01:22:01', NULL),
(11, 'awaiting', 6, 9800, '2019-05-28 09:43:47', 1, '2018-05-10 03:31:47', '2018-06-10 03:31:47', 'saudia', 0, 0, '83', '2019-05-29 01:22:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_ar` varchar(225) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL DEFAULT '',
  `question_en` varchar(225) NOT NULL DEFAULT '',
  `answer_ar` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `answer_en` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_ar`, `question_en`, `answer_ar`, `answer_en`) VALUES
(1, 'ما سعر الجمال ؟', 'You have double rooms ?', 'حسب الوزن والكميه تبدا من ١٠ الاف الي ١٥ الف', 'Yes'),
(2, 'كيف يمكنني التواصل  معكم\r\n؟', 'How can we start to work hard ?', 'عن طريق ارقامنا او السوشيال ميديا نحن نمتلك حسابات علي كل من تويتر فيس بوك ', 'Through our phone or social media we have accounts at Facebook and twitter'),
(3, 'متي تبدآ ساعات العمل ؟', 'when does work end ?', 'نحن نعمل ٢٤ ساعه ', 'Never ends'),
(5, 'هل يوجد لديكوا تسويه ؟', 'Should I pay cash ?', 'نعم يوجد لدينا كل شئ', 'No, you can buy with anyway not just cash'),
(6, 'سؤال 1', 'question', 'إجابة', 'answer');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `item_id`, `user_id`, `rate`) VALUES
(8, 2, 10, 2.5),
(9, 4, 10, 4.5),
(10, 2, 10, 4.3),
(12, 4, 10, 1.5),
(13, 2, 10, 3.5),
(18, 3, 10, 4),
(19, 4, 10, 1),
(20, 2, 10, 1),
(21, 1, 10, 4),
(22, 1, 10, 3),
(23, 2, 10, 2),
(24, 4, 10, 2),
(25, 3, 10, 5),
(26, 3, 10, 1),
(27, 4, 10, 3.5);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

DROP TABLE IF EXISTS `social_media`;
CREATE TABLE IF NOT EXISTS `social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(225) NOT NULL DEFAULT '',
  `details_ar` varchar(225) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `details_en` varchar(225) NOT NULL DEFAULT '',
  `link` varchar(225) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `image`, `details_ar`, `details_en`, `link`) VALUES
(1, 'telgram.png', 'Telgram', 'Telgram', 'http://telgram.com'),
(2, 'snapchat.png', 'Snap Chat', 'Snap Chat', 'http://www.snapchat.com'),
(3, 'twitter.png', 'Twitter', 'Twitter', 'http://www.twitter.com'),
(4, 'instgram.png', 'Instgram', 'Instgram', 'http://www.instgram.com'),
(5, 'pinterest.png', 'Pinterest', 'Pinterest', 'http://www.pinterest.com'),
(6, 'slack.png', 'Slack', 'Slack', 'https://slack.com');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `name_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `category_id`, `user_id`, `name_en`, `name_ar`, `image`) VALUES
(1, 15, 1, 'الديزل', 'الديزل', 'store1.jpg'),
(2, 8, 1, 'العمده', 'العمده', 'store2.jpg'),
(3, 8, 20, 'الشيخ ', 'الشيخ ', 'store3.jpg'),
(4, 8, 20, 'دريم', 'دريم', 'store4.jpg'),
(5, 0, 0, '1', '1', ''),
(6, 0, 0, '1', '1', ''),
(7, 0, 0, '1', '1', ''),
(8, 0, 0, '1', '1', ''),
(9, 0, 0, '1', '1', ''),
(10, 0, 0, '1', '1', ''),
(11, 0, 0, '1', '1', ''),
(12, 0, 0, '1', '1', ''),
(13, 0, 0, '1', '1', ''),
(14, 0, 0, '1', '1', ''),
(15, 0, 0, '1', '1', ''),
(16, 0, 0, '1', '1', ''),
(17, 0, 0, '1', '1', ''),
(18, 0, 0, '1', '1', ''),
(19, 0, 0, '1', '1', ''),
(20, 0, 0, '1', '1', ''),
(21, 0, 0, '1', '1', ''),
(22, 0, 0, '1', '1', ''),
(23, 0, 0, '1', '1', ''),
(24, 0, 0, '1', '1', ''),
(25, 0, 0, '1', '1', ''),
(26, 0, 0, '1', '1', ''),
(27, 0, 0, '1', '1', ''),
(28, 0, 0, '1', '1', ''),
(29, 0, 0, '1', '1', ''),
(30, 0, 0, '1', '1', ''),
(31, 0, 0, '1', '1', ''),
(32, 0, 0, '1', '1', ''),
(33, 0, 0, '1', '1', ''),
(34, 0, 0, '1', '1', ''),
(35, 0, 0, '1', '1', ''),
(36, 0, 0, '1', '1', ''),
(37, 0, 0, '1', '1', ''),
(38, 0, 0, '1', '1', ''),
(39, 0, 0, '1', '1', ''),
(40, 0, 0, '1', '1', ''),
(41, 0, 0, '1', '1', ''),
(42, 0, 0, '1', '1', ''),
(43, 0, 0, '1', '1', ''),
(44, 0, 0, '1', '1', ''),
(45, 0, 0, '1', '1', ''),
(46, 0, 0, '1', '1', ''),
(47, 0, 0, '1', '1', ''),
(48, 15, 0, 'store name', 'store name', ''),
(49, 15, 0, 'store name', 'store name', ''),
(50, 15, 0, 'store name', 'store name', ''),
(51, 15, 0, 'my store', 'my store', '');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
CREATE TABLE IF NOT EXISTS `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_ar` varchar(225) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `title_en` varchar(225) NOT NULL DEFAULT '',
  `details_ar` text CHARACTER SET utf8 NOT NULL,
  `details_en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`) VALUES
(1, 'عنوان عنوان', 'title title', 'يعتبر تطبيق الذبائح من اسهل الطرق لشراء الذبائح ا،ن لاين و بكل سهوله و سلاسه', 'Elmasry Gym one of the best Gyms at Egypt\r\nWe have amazing trainers \r\nWe sell everything');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('admin','user') NOT NULL DEFAULT 'user',
  `name` varchar(225) NOT NULL DEFAULT '',
  `image` varchar(225) NOT NULL DEFAULT 'default.png',
  `phone` varchar(225) NOT NULL DEFAULT '',
  `email` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `google_id` varchar(225) DEFAULT NULL,
  `device_token` varchar(225) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lang` double DEFAULT '0',
  `address` text,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type`, `name`, `image`, `phone`, `email`, `password`, `google_id`, `device_token`, `lat`, `lang`, `address`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Mohamed', '', '01121805383', 'admin@admin.com', '4297f44b13955235245b2497399d7a93', NULL, NULL, 0, 0, 'gamal and alnas', '2019-05-28 00:00:00', NULL),
(2, 'user', 'mohamed atef', '', '01121904323', 'moh@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, 0, 'gmal abd alnasser', '2019-05-28 00:00:00', NULL),
(3, 'user', 'memo', '', '01121943562', 'a@i.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, 0, 'addresszes', '2019-05-28 00:00:00', NULL),
(4, 'user', 'memo', '', '0122194356', 'a@i.com2', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, 0, 'addresszes', '2019-05-28 00:00:00', NULL),
(5, 'user', 'trader', '', '011231805383', 'trader@email.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, 0, 'gmal abd alnasser street', '2019-05-28 00:00:00', NULL),
(6, 'user', 'mohamed', '', '01121805483', 'memo@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, 0, 'gamal', '2019-05-28 00:00:00', NULL),
(8, 'admin', 'Admin 2', 'default.png', '08544646', 'admin2@admin.com', '4297f44b13955235245b2497399d7a93', NULL, NULL, 0, 0, 'عنوان عنوان عنوان', '2019-05-28 10:15:09', '2019-05-28 08:15:09');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
