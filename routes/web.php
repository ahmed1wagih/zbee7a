<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\IsAdmin;

Route::get('/', function ()
{
    return redirect('/admin/dashboard');
});

Route::group(['prefix' => '/admin'], function()
{
    Route::get('/login', 'Admin\AuthController@login_view');
    Route::post('/login', 'Admin\AuthController@login');
    Route::get('/logout', 'Admin\AuthController@logout');

    Route::group(['middleware' =>IsAdmin::class], function()
    {
        Route::get('/dashboard', 'Admin\HomeController@index');

        Route::get('/admins/index', 'Admin\UserController@admins');
        Route::get('/users/index', 'Admin\UserController@users');
        Route::get('/users/search', 'Admin\UserController@search');
        Route::get('/user/create', 'Admin\UserController@create');
        Route::post('/user/store', 'Admin\UserController@store');
        Route::get('/user/{id}/edit', 'Admin\UserController@edit');
        Route::post('/user/update', 'Admin\UserController@update');
        Route::post('/user/delete', 'Admin\UserController@destroy');

        Route::get('/cuttings/index', 'Admin\CuttingController@index');
        Route::get('/cutting/create', 'Admin\CuttingController@create');
        Route::post('/cutting/store', 'Admin\CuttingController@store');
        Route::get('/cutting/{id}/edit', 'Admin\CuttingController@edit');
        Route::post('/cutting/update', 'Admin\CuttingController@update');

        Route::get('/packagings/index', 'Admin\PackagingController@index');
        Route::get('/packaging/create', 'Admin\PackagingController@create');
        Route::post('/packaging/store', 'Admin\PackagingController@store');
        Route::get('/packaging/{id}/edit', 'Admin\PackagingController@edit');
        Route::post('/packaging/update', 'Admin\PackagingController@update');

        Route::get('/sizes/index', 'Admin\SizingController@index');
        Route::get('/size/create', 'Admin\SizingController@create');
        Route::post('/size/store', 'Admin\SizingController@store');
        Route::get('/size/{id}/edit', 'Admin\SizingController@edit');
        Route::post('/size/update', 'Admin\SizingController@update');

        Route::get('/items/index', 'Admin\ItemController@index');
        Route::get('/items/search', 'Admin\ItemController@search');
        Route::get('/item/create', 'Admin\ItemController@create');
        Route::post('/item/store', 'Admin\ItemController@store');
        Route::get('/item/{id}/edit', 'Admin\ItemController@edit');
        Route::post('/item/update', 'Admin\ItemController@update');
        Route::post('/item/delete', 'Admin\ItemController@destroy');

        Route::get('/orders/{status}/index', 'Admin\OrderController@index');
        Route::get('/orders/search', 'Admin\OrderController@search');
        Route::get('/order/{id}/show', 'Admin\OrderController@show');
        Route::get('/order/user/{id}/list', 'Admin\OrderController@user_list');
        Route::post('/order/change_status', 'Admin\OrderController@change_status');
        Route::post('/order/delete', 'Admin\OrderController@destroy');


        Route::get('/messages/index', 'Admin\MessageController@index');
        Route::post('/message/delete', 'Admin\MessageController@destroy');


        Route::group(['prefix' => '/settings'], function()
        {
            Route::get('/sliders/index', 'Admin\SliderController@index');
            Route::get('/slider/create', 'Admin\SliderController@create');
            Route::post('/slider/store', 'Admin\SliderController@store');
            Route::get('/slider/{id}/edit', 'Admin\SliderController@edit');
            Route::post('/slider/update', 'Admin\SliderController@update');
            Route::post('/slider/delete', 'Admin\SliderController@destroy');

            Route::get('/about/index', 'Admin\AboutController@index');
            Route::get('/about/edit', 'Admin\AboutController@edit');
            Route::post('/about/update', 'Admin\AboutController@update');

            Route::get('/faqs/index', 'Admin\FaqController@index');
            Route::get('/faq/create', 'Admin\FaqController@create');
            Route::post('/faq/store', 'Admin\FaqController@store');
            Route::get('/faq/{id}/edit', 'Admin\FaqController@edit');
            Route::post('/faq/update', 'Admin\FaqController@update');
            Route::post('/faq/delete', 'Admin\FaqController@destroy');

            Route::get('/terms/index', 'Admin\TermController@index');
            Route::get('/terms/edit', 'Admin\TermController@edit');
            Route::post('/terms/update', 'Admin\TermController@update');

            Route::get('/notifications/index', 'Admin\NotificationController@index');
            Route::post('/notification/store', 'Admin\NotificationController@store');
            Route::post('/notification/delete', 'Admin\NotificationController@destroy');
        });
    });
});
