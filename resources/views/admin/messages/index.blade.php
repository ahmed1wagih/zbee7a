@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">الشكاوي و الإقتراحات</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">المستخدم</th>
                        <th class="rtl_th">الرسالة</th>
                        <th class="rtl_th">الهاتف أو البريد الإلكتروني</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                    <tr>
                        <td>{{$message->user->name}}</td>
                        <td>{{$message->message}}</td>
                        <td>{{$message->phone_or_mail}}</td>

                        <td>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$message->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$message->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذه الرسالة و لن تستطيع إسترجاع بياناتها مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/message/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="message_id" value="{{$message->id}}">
                                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$messages->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
