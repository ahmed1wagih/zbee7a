<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>ذبيحة - لوحة التحكم</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('admin/assets/images/users/avatar.jpg')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/theme-default_rtl.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/rtl.css')}}"/>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->


    <style>

        .image_radius
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container page-mode-rtl page-content-rtl">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll" style="height: 0px !important">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/admin/dashboard">ذبيحة</a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">

                <div class="profile">
                    <div class="profile-image">
                        <img src="{{asset('admin/assets/images/users/avatar.jpg')}}" alt="ذبيحة"/>
                    </div>
                </div>
            </li>
            <li class="@if(Request::is('admin/dashboard/*')) active @endif">
                <a href="/admin/dashboard"><span class="xn-text">الرئيسية</span> <span class="fa fa-home"></span></a>
            </li>
            <li class="xn-openable @if(Request::is('admin/admins/index') || Request::is('admin/users/index') || Request::is('admin/user/*')) active @endif" >
                <a href="#"><span class="xn-text">المستخدمين</span> <span class="fa fa-group"></span></a>
                <ul>
                    <li @if(Request::is('admin/admins/index')) class="active" @endif>
                        <a href="/admin/admins/index"><span class="xn-text">المديرين</span><span class="fa fa-user-secret"></span></a>
                    </li>
                    <li @if(Request::is('admin/users/index')) class="active" @endif>
                        <a href="/admin/users/index"><span class="xn-text">المستخدمين</span><span class="fa fa-users"></span></a>
                    </li>
                </ul>
            </li>
            <li class="@if(Request::is('admin/cuttings/*') || Request::is('admin/cutting/*')) active @endif">
                <a href="/admin/cuttings/index"><span class="xn-text">أنواع التقطيع</span> <span class="fa fa-info-circle"></span></a>
            </li>
            <li class="@if(Request::is('admin/packagings/*') || Request::is('admin/packaging/*')) active @endif">
                <a href="/admin/packagings/index"><span class="xn-text">أنواع التغليف</span> <span class="fa fa-info-circle"></span></a>
            </li>
            <li class="@if(Request::is('admin/sizes/*') || Request::is('admin/size/*')) active @endif">
                <a href="/admin/sizes/index"><span class="xn-text">أنواع الحجم</span> <span class="fa fa-info-circle"></span></a>
            </li>
            <li class="@if(Request::is('admin/items/*') || Request::is('admin/item/*')) active @endif">
                <a href="/admin/items/index"><span class="xn-text">المنتجات</span> <span class="fa fa-cubes"></span></a>
            </li>
            <li class="xn-openable @if(Request::is('admin/orders/*') || Request::is('admin/order/*')) active @endif" >
                <a href="#"><span class="xn-text">طلبات الشراء</span> <span class="fa fa-shopping-basket"></span></a>
                <ul>
                    <li @if(Request::is('admin/orders/awaiting/index')) class="active" @endif>
                        <a href="/admin/orders/awaiting/index"><span class="xn-text">قيد الإنتظار</span><span class="fa fa-clock-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/orders/completed/index')) class="active" @endif>
                        <a href="/admin/orders/completed/index"><span class="xn-text">منتهية</span><span class="fa fa-check-circle"></span></a>
                    </li>
                </ul>
            </li>
            <li class="@if(Request::is('admin/messages/*')) active @endif">
                <a href="/admin/messages/index"><span class="xn-text">الشكاوي و المقترحات</span> <span class="fa fa-mail-forward"></span></a>
            </li>
            <li class="xn-openable @if(Request::is('admin/settings/*')) active @endif">
                <a href="#"><span class="xn-text">الإعدادات</span> <span class="fa fa-shopping-basket"></span></a>
                <ul>
                    <li @if(Request::is('admin/settings/sliders/*')) class="active" @endif>
                        <a href="/admin/settings/sliders/index"><span class="xn-text">سلايدر الصور</span><span class="fa fa-picture-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/settings/about/*')) class="active" @endif>
                        <a href="/admin/settings/about/index"><span class="xn-text">معلومات عنا</span><span class="fa fa-info-circle"></span></a>
                    </li>
                    <li @if(Request::is('admin/settings/faqs/*')) class="active" @endif>
                        <a href="/admin/settings/faqs/index"><span class="xn-text">الأسئلة الشائعة</span><span class="fa fa-info-circle"></span></a>
                    </li>
                    <li @if(Request::is('admin/settings/terms/*')) class="active" @endif>
                        <a href="/admin/settings/terms/index"><span class="xn-text">شروط الإستخدام</span><span class="fa fa-info-circle"></span></a>
                    </li>
                    <li @if(Request::is('admin/settings/notifications/*')) class="active" @endif>
                        <a href="/admin/settings/notifications/index"><span class="xn-text">الإشعارات العامة</span><span class="fa fa-info-circle"></span></a>
                    </li>
                </ul>
            </li>


        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- POWER OFF -->
            <li class="xn-icon-button last">
                <a href="#" class="mb-control" data-box="#mb-signout" title="Logout"><span class="fa fa-power-off"></span></a>
            </li>
            <!-- END POWER OFF -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->


        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> تسجيل الخروج ؟</div>
                    <div class="mb-content">
                        <p>هل أنت متأكد من تسجيل الخروج ؟</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/admin/logout" class="btn btn-success btn-lg">نعم</a>
                            <button class="btn btn-default btn-lg mb-control-close">لا</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

     @yield('content')





<!-- START PRELOADS -->
<audio id="audio-alert" src="{{asset('admin/audio/alert.mp3')}}" preload="auto"></audio>
<audio id="audio-fail" src="{{asset('admin/audio/fail.mp3')}}" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/owl/owl.carousel.min.js')}}"></script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="{{asset('admin/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/actions.js')}}"></script>


<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
<!-- END THIS PAGE PLUGINS -->
<!-- END SCRIPTS -->
</body>
</html>






