@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>أنواع التقطيع</li>
        <li class="active">
            @if(isset($cutting))
                تعديل نوع
            @else
                إضافة نوع
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($cutting)) action="/admin/cutting/update" @else action="/admin/cutting/store" @endif>
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    @if(isset($cutting))
                                        تعديل نوع
                                    @else
                                        إضافة نوع
                                    @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($cutting)) value="{{$cutting->ar_name}}" @else value="{{old('ar_name')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($cutting)) value="{{$cutting->en_name}}" @else value="{{old('en_name')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>
                            
                            @if(isset($cutting))
                                <input type="hidden" name="id" value="{{$cutting->id}}">
                            @endif
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($cutting))
                                    تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    
@endsection
