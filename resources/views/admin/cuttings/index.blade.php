@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">أنواع التقطيع</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/cutting/create">
            <button type="button" class="btn btn-info">أضف نوع جديد</button>
            </a>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">الإسم بالعربية</th>
                        <th class="rtl_th">الإسم بالإنجليزية</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cuttings as $cutting)
                    <tr>
                        <td>{{$cutting->ar_name}}</td>
                        <td>{{$cutting->en_name}}</td>
                        <td>
                            <a href="/admin/cutting/{{$cutting->id}}/edit"><button class="btn btn-condensed btn-warning" title="تعديل"><i class="fa fa-edit"></i></button></a>
{{--                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$cutting->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>--}}
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$cutting->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا النوع و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/cutting/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$cutting->id}}">
                                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
