@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات</li>
        <li>شروط الإستخدام</li>
        <li class="active">
           تعديل
        </li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/admin/settings/terms/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                  تعديل
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('title_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                       <textarea name="title_ar" class="form-control" rows="5">{{$text->title_ar}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('title_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea name="title_en" class="form-control" rows="5">{{$text->title_en}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title_en'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('details_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea name="details_ar" class="form-control" rows="10">{{$text->details_ar}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'details_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('details_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea name="details_en" class="form-control" rows="10">{{$text->details_en}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'details_en'])
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                               تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    
@endsection
