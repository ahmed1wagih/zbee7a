@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">طلبات الشراء</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <form class="form-horizontal" method="get" action="/admin/orders/search">
            <div class="form-group">
                <div class="col-md-6 col-xs-12">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon btn btn-default">
                            <button class="btn btn-default" style="margin-right: 5px;">إبحث الآن</button>
                        </span>
                        <input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="إبحث بإسم العميل,البريد الألكتروني أو الهاتف" style="margin-top: 1px;"/>

                    </div>
                </div>
            </div>
        </form>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        @if(isset($search))
                            <th class="rtl_th">الحالة</th>
                        @endif
                        <th class="rtl_th">العميل</th>
                        <th class="rtl_th">من وقت</th>
                        <th class="rtl_th">إلي</th>
                        <th class="rtl_th">العنوان</th>
                        <th class="rtl_th">إجمال التكلفة</th>
                        <th class="rtl_th">عدد المنتجات</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                    <tr>
                        @if(isset($search))
                            <td>{{$order->status == 'awaiting' ? 'قيد الإنتظار' : 'منتهي'}}</td>
                        @endif
                        <td>{{$order->user->name}}</td>
                        <td>{{$order->from}}</td>
                        <td>{{$order->to}}</td>
                        <td>{{$order->address}}</td>
                        <td>{{$order->final_price}}</td>
                        <td>{{$order->get_items_count($order->id)}}</td>
                        <td>
                            <a href="/admin/order/{{$order->id}}/show"><button class="btn btn-condensed btn-info" title="مشاهدة"><i class="fa fa-eye"></i></button></a>
                            @if($order->status == 'awaiting')
                                <form method="post" action="/admin/order/change_status" class="buttons">
                                    {{csrf_field()}}
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <button type="submit" class="btn btn-success" title="إنهاء الطلب"><i class="fa fa-check-circle"></i></button>
                                </form>
                            @endif
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$order->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$order->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا الطلب و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/order/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$orders->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
