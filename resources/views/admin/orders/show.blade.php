@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>طلبات الشراء</li>
        <li class="active">مشاهدة طلب</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="direction: rtl;">
    @include('admin.layouts.message')
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>الطلب رقم<strong> #{{$order->id}}</strong></h1>

                        <!-- INVOICE -->
                        <div class="invoice">

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="invoice-address">
                                        <h1>العميل</h1>
                                        <h3>{{$order->user->name}}</h3>
                                        <h2>رقم الهاتف: {{$order->user->phone}}</h2>
                                        <h2>العنوان : {{$order->address}}</h2>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="invoice-address">
                                        <h1>الفاتورة</h1>
                                        <table class="table table-striped">
                                            <tr>
                                                <td><h2> تاريخ الطلب :</h2></td><td class="text-left" style="direction: ltr;"><h2>{{\Carbon\Carbon::parse($order->created_at)->format('j M Y , g:ia')}}</h2></td>
                                            </tr>

                                            <tr>
                                                <td><h2> موعد التوصيل من :</h2></td><td class="text-left" style="direction: ltr;"><h2>{{\Carbon\Carbon::parse($order->from)->format('j M Y , g:ia')}}</h2></td>
                                            </tr>

                                            <tr>
                                                <td><h2> إلي :</h2></td><td class="text-left" style="direction: ltr;"><h2>{{\Carbon\Carbon::parse($order->to)->format('j M Y , g:ia')}}</h2></td>
                                            </tr>
                                        </table>

                                    </div>

                                </div>
                            </div>

                            <!-- BLUEIMP GALLERY -->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                            <!-- END BLUEIMP GALLERY -->

                                <div class="table-invoice">
                                    <table class="table">
                                        <tr>
                                            <th><h2>إسم المنتج</h2></th>
                                            <th><h2>السعر قبل</h2></th>
                                            <th><h2>بعد</h2></th>
                                            <th><h2>الصورة</h2></th>
                                        </tr>

                                        @foreach($order->get_items($order->basket_items) as $item)
                                            <tr>
                                                <td>
                                                    <h3>{{$item->title_ar}}</h3>
                                                    <p>{{$item->details_ar}}</p>
                                                </td>
                                                <td><h3>{{$item->price_before}}</h3></td>
                                                <td><h3>{{$item->price_after}}</h3></td>
                                                <td><img src="/items/{{$item->cover->image}}" class="image_radius"/></td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right">{{$order->item_total}}</td>
                                        </tr>
                                    </table>
                                </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <h1>الإجمالي</h1>

                                    <table class="table table-striped">

                                        <tr class="total">
                                            <td><h3 style="color: white;">{{$order->final_price}}</h3></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END INVOICE -->
                        <div class="panel-footer">
                            @if($order->status == 'awaiting')
                                <form method="post" action="/admin/order/change_status">
                                    {{csrf_field()}}
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <button class="btn btn-success pull-right" style="font-size: 25px;">
                                        إنهاء
                                    </button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <script>
        document.getElementById('links').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement;
            var link = target.src ? target.parentNode : target;
            var options = {index: link, event: event,onclosed: function(){
                    setTimeout(function(){
                        $("body").css("overflow","");
                    },200);
                }};
            var links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>

@endsection
