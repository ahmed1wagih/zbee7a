@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>مستخدمين التطبيق</a></li>
        <li class="active">
            @if(isset($user))
                تعديل
            @else
                إضافة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($user)) action="/admin/user/update" @else action="/admin/user/store" @endif>
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    @if(isset($user))
                                        تعديل مستخدم
                                    @else
                                        إضافة مستخدم
                                    @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">النوع</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <select class="form-control select" name="type">
                                            <option value="user" @if(isset($user) && $user->type == 'user') selected @endif>مستخدم عادي</option>
                                            <option value="admin" @if(isset($user) && $user->type == 'admin') selected @endif>مدير</option>
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'type'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" class="form-control" name="name" @if(isset($user)) value="{{$user->name}}" @else value="{{old('name')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="email" class="form-control dont_fill" name="email" @if(isset($user)) value="{{$user->email}}" @else value="{{old('email')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'email'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">رقم الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="text" class="form-control" name="phone" @if(isset($user)) value="{{$user->phone}}" @else value="{{old('phone')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'phone'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input autocomplete="off" type="password" class="form-control dont_fill" name="password"/>
                                    </div>
                                    @if(isset($user))
                                        <p style="color: red;">إتركه فارغاً إذا لا تريد التعديل</p>
                                    @endif
                                    @include('admin.layouts.error', ['input' => 'password'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input autocomplete="off" type="password" class="form-control dont_fill" name="password_confirmation"/>
                                    </div>
                                    @if(isset($user))
                                        <p style="color: red;">إتركه فارغاً إذا لا تريد التعديل</p>
                                    @endif
                                    @include('admin.layouts.error', ['input' => 'password_confirmation'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-map"></span></span>
                                        <input type="text" class="form-control" name="address" @if(isset($user)) value="{{$user->address}}" @else value="{{old('address')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'address'])
                                </div>
                            </div>

                            @if(isset($user))
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                            @endif

                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($user))
                                   تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script>
        setTimeout(
            function() { $(':password').val(''); },
            1000  //1,000 milliseconds = 1 second
        );
    </script>
@endsection
