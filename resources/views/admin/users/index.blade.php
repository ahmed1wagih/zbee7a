@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">المستخدمين </li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/user/create">
            <button type="button" class="btn btn-info">أضف مستخدم</button>
            </a>
        </div>
        <form class="form-horizontal" method="get" action="/admin/users/search">
            <div class="form-group">
                <div class="col-md-6 col-xs-12">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon btn btn-default">
                            <button class="btn btn-default" style="margin-right: 5px;">إبحث الآن</button>
                        </span>
                        <input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="إبحث بالإسم,البريد أو الهاتف" style="margin-top: 1px;"/>

                    </div>
                </div>
            </div>
        </form>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">#</th>
                        @if(isset($search))
                        <th class="rtl_th">النوع</th>
                        @endif
                        <th class="rtl_th">الإسم</th>
                        <th class="rtl_th">البريد الإلكتروني</th>
                        <th class="rtl_th">رقم الهاتف</th>
                        <th class="rtl_th">عدد الطلبات</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        @if(isset($search))
                            <td>{{$user->type == 'user' ? 'مستخدم عادي' : 'مدير'}}</td>
                        @endif
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->orders->count()}}</td>
                        <td>
                            <a href="/admin/order/user/{{$user->id}}/list"><button class="btn btn-condensed btn-info" title="مشاهدة الطلبات"><i class="fa fa-cubes"></i></button></a>
                            <a href="/admin/user/{{$user->id}}/edit"><button class="btn btn-condensed btn-warning" title="تعديل"><i class="fa fa-edit"></i></button></a>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$user->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$user->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا المستخدم و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/user/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$users->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
