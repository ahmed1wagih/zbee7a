@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات</li>
        <li>الأسئلة الشائعة</li>
        <li class="active">
            @if(isset($user))
                تعديل مستخدم
            @else
                إضافة مستخدم
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($faq)) action="/admin/settings/faq/update" @else action="/admin/settings/faq/store" @endif>
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    @if(isset($user))
                                        تعديل مستخدم
                                    @else
                                        إضافة مستخدم
                                    @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('question_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السؤال بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="question_ar" @if(isset($faq)) value="{{$faq->question_ar}}" @else value="{{old('question_ar')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'question_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('question_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السؤال بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="question_en" @if(isset($faq)) value="{{$faq->question_en}}" @else value="{{old('question_en')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'question_en'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('answer_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإجابة بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="answer_ar" @if(isset($faq)) value="{{$faq->answer_ar}}" @else value="{{old('answer_ar')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'answer_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('answer_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإجابة بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="answer_en" @if(isset($faq)) value="{{$faq->answer_en}}" @else value="{{old('answer_en')}}" @endif>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'answer_en'])
                                </div>
                            </div>

                            @if(isset($faq))
                                <input type="hidden" name="question_id" value="{{$faq->id}}">
                            @endif
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($faq))
                                    تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    
@endsection
