@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">المنتجات</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/item/create">
            <button type="button" class="btn btn-info">أضف منتج</button>
            </a>
        </div>
        <form class="form-horizontal" method="get" action="/admin/items/search">
            <div class="form-group">
                <div class="col-md-6 col-xs-12">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon btn btn-default">
                            <button class="btn btn-default" style="margin-right: 5px;">إبحث الآن</button>
                        </span>
                        <input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="إبحث بالإسم العربي أو الإنجليزي" style="margin-top: 1px;"/>

                    </div>
                </div>
            </div>
        </form>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">#</th>
                        <th class="rtl_th">العنوان بالعربية</th>
                        <th class="rtl_th">العنوان بالإنجليزية</th>
                        <th class="rtl_th">الوصف بالعربية</th>
                        <th class="rtl_th">الوصف بالإنجليزية</th>
                        <th class="rtl_th">السعر قبل</th>
                        <th class="rtl_th">السعر بعد</th>
                        <th class="rtl_th">التقطيع</th>
                        <th class="rtl_th">التغليف</th>
                        <th class="rtl_th">الحجم</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->title_ar}}</td>
                        <td>{{$item->title_en}}</td>
                        <td>{{$item->details_ar}}</td>
                        <td>{{$item->details_en}}</td>
                        <td>{{$item->price_before}}</td>
                        <td>{{$item->price_after}}</td>
                        <td>{{$item->cutting->ar_name}}</td>
                        <td>{{$item->packaging->ar_name}}</td>
                        <td>{{$item->size->ar_name}}</td>
                        <td>
                            <a href="/admin/item/{{$item->id}}/edit"><button class="btn btn-condensed btn-warning"><i class="fa fa-edit"></i></button></a>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$item->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$item->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا المنتج و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/item/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="item_id" value="{{$item->id}}">
                                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$items->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
