@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>المنتجات</li>
        <li class="active">
            @if(isset($item))
                تعديل
            @else
                إضافة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($item)) action="/admin/item/update" @else action="/admin/item/store" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    @if(isset($item))
                                        تعديل منتح
                                    @else
                                        إضافة منتح
                                    @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('title_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title_ar" @if(isset($item)) value="{{$item->title_ar}}" @else value="{{old('title_ar')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('title_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title_en" @if(isset($item)) value="{{$item->title_en}}" @else value="{{old('title_en')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title_en'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('details_ar') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="details_ar" @if(isset($item)) value="{{$item->details_ar}}" @else value="{{old('details_ar')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'details_ar'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('details_en') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="details_en" @if(isset($item)) value="{{$item->details_en}}" @else value="{{old('details_en')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'details_en'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('price_before') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر قبل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" step="1" class="form-control" name="price_before" @if(isset($item)) value="{{$item->price_before}}" @else value="{{old('price_before')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'price_before'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('price_after') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر بعد</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" step="1" class="form-control" name="price_after" @if(isset($item)) value="{{$item->price_after}}" @else value="{{old('price_after')}}" @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'price_after'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('cutting_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">نوع التقطيع</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <select class="form-control select" name="cutting_id" title="إختر نوع">
                                            @foreach($cuttings as $cutting)
                                                <option value="{{$cutting->id}}" @if(isset($item) && $item->cutting_id == $cutting->id) selected @endif>{{$cutting->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'cutting_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('packaging_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">نوع التغليف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <select class="form-control select" name="packaging_id" title="إختر نوع">
                                            @foreach($packagings as $packaging)
                                                <option value="{{$packaging->id}}" @if(isset($item) && $item->packaging_id == $packaging->id) selected @endif>{{$packaging->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'packaging_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('size_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">نوع الحجم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <select class="form-control select" name="size_id" title="إختر نوع">
                                            @foreach($sizes as $size)
                                                <option value="{{$size->id}}" @if(isset($item) && $item->size_id == $size->id) selected @endif>{{$size->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'size_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">الصور</label>
                             <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <input type="file" class="fileinput btn-info" name="images[]" multiple id="cp_photo" data-filename-placement="inside" title="{{isset($item) ? 'أضف مزيد من الصور' : 'أضف صور'}}" @if(!isset($item)) required @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'images'])
                                </div>

                                @if(isset($item->images))
                                    <div class="gallery" id="links">
                                        @foreach($item->images as $image)
                                            <a href="/items/{{$image->image}}" title="{{$image->image}}" class="gallery-item" data-gallery>
                                                <div class="image">
                                                    <img class="img" src="/items/{{$image->image}}" alt="{{$image->image}}"/>
                                                </div>
                                                @if($item->images->count() > 1)
                                                    <label class="check" title="حذف الصورة"><input type="checkbox" name="deleted_ids[]" value="{{$image->id}}" class="icheckbox" style="border: black solid 1px !important;"/></label>
                                                @endif
                                            </a>
                                        @endforeach
                                    </div>
                                @endif

                            </div>

                            @if(isset($item))
                                <input type="hidden" name="item_id" value="{{$item->id}}">
                            @endif

                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($item))
                                   تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>

    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->

    @if(isset($item))
        <script>
            document.getElementById('links').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {index: link, event: event,onclosed: function(){
                            setTimeout(function(){
                                $("body").css("overflow","");
                            },200);
                        }},
                    links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };
        </script>
    @endif
@endsection
