@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات</li>
        <li class="active">معلومات عنا</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">العنوان بالعربية</th>
                        <th class="rtl_th">العنوان بالإنجليزية</th>
                        <th class="rtl_th">التفاصيل بالعربية</th>
                        <th class="rtl_th">التفاصيل بالإنجليزية</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$text->title_ar}}</td>
                        <td>{{$text->title_en}}</td>
                        <td>{{$text->details_ar}}</td>
                        <td>{{$text->details_en}}</td>
                        <td>
                            <a href="/admin/settings/about/edit"><button class="btn btn-condensed btn-warning" title="تعديل"><i class="fa fa-edit"></i></button></a>
                        </td>
                    </tr>
                    </tbody>

                </table>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
