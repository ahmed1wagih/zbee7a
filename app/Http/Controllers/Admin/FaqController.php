<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::get();
        return view('admin.faqs.index', compact('faqs'));
    }


    public function create()
    {
        return view('admin.faqs.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'question_ar' => 'required',
                'question_en' => 'required',
                'answer_ar' => 'required',
                'answer_en' => 'required',
            ],
            [
                'question_ar.required' => 'السؤال بالعربية مطلوب',
                'question_en.required' => 'السؤال بالإنجليزية مطلوب',
                'answer_ar.required' => 'الإجابة بالعربية مطلوبة',
                'answer_en.required' => 'الإجابة بالإنجليزية مطلوبة',
            ]
        );

        Faq::create($request->all());

        return redirect('/admin/settings/faqs/index')->with('success', 'تمت الإضافة بنجاح');
    }


    public function edit($id, Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'required|exists:questions,id',
            ]
        );

        $faq = Faq::find($id);
        return view('admin.faqs.single', compact('faq'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'question_id' => 'required|exists:questions,id',
                'question_ar' => 'required',
                'question_en' => 'required',
                'answer_ar' => 'required',
                'answer_en' => 'required',
            ],
            [
                'question_ar.required' => 'السؤال بالعربية مطلوب',
                'question_en.required' => 'السؤال بالإنجليزية مطلوب',
                'answer_ar.required' => 'الإجابة بالعربية مطلوبة',
                'answer_en.required' => 'الإجابة بالإنجليزية مطلوبة',
            ]
        );

        $faq = Faq::find($request->question_id);
            $faq->question_ar = $request->question_ar;
            $faq->question_en = $request->question_en;
            $faq->answer_ar = $request->answer_ar;
            $faq->answer_en = $request->answer_en;
        $faq->save();

        return redirect('/admin/settings/faqs/index')->with('success', 'تم التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'question_id' => 'required|exists:questions,id',
            ]
        );


        Faq::where('id', $request->question_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
