<?php

namespace App\Http\Controllers\Admin;

use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TermController extends Controller
{
    public function index()
    {
        $text = Term::first();
        return view('admin.terms.index', compact('text'));
    }


    public function edit()
    {
        $text = Term::first();
        return view('admin.terms.single', compact('text'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'title_ar' => 'required',
                'title_en' => 'required',
                'details_ar' => 'required',
                'details_en' => 'required',
            ],
            [
                'title_ar.required' => 'العنوان بالعربية مطلوب',
                'title_en.required' => 'العنوان بالإنجليزية مطلوب',
                'details_ar.required' => 'التفاصيل بالعربية مطلوبة',
                'details_en.required' => 'التفاصيل بالإنجليزية مطلوبة',
            ]
        );

        $text = Term::first();
            $text->title_ar = $request->title_ar;
            $text->title_en = $request->title_en;
            $text->details_ar = $request->details_ar;
            $text->details_en = $request->details_en;
        $text->save();

        return redirect('/admin/settings/terms/index')->with('success', 'تم التعديل بنجاح');
    }
}
