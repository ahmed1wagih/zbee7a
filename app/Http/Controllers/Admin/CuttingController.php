<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cutting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CuttingController extends Controller
{
    public function index()
    {
        $cuttings = Cutting::get();
        return view('admin.cuttings.index', compact('cuttings'));
    }


    public function create()
    {
        return view('admin.cuttings.single');
    }



    public function store(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Cutting::create($request->all());

        return redirect('/admin/cuttings/index')->with('success','تم إضافة تقطيع جديد بنجاح');
    }


    public function edit($id)
    {
        $cutting = Cutting::find($id);
        return view('admin.cuttings.single', compact('cutting'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:cuttings,id',
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Cutting::find($request->id)->update($request->all());

        return redirect('/admin/cuttings/index')->with('success','تم تعديل التقطيع بنجاح');
    }
}
