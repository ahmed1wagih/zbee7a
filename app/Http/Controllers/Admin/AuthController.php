<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login_view()
    {
        return view('admin.auth.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );

        $user = User::where('email', $request->email)->where('type', 'admin')->first();

        if($user)
        {
            if(md5($request->password) == $user->password)
            {
                Auth::loginUsingId($user->id);
                return redirect('/admin/dashboard');
            }
            else
            {
                return back()->with('error','كلمة مرور خاطئة');
            }
        }
        else
        {
            return back()->with('error','بريد خاطئ');
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
