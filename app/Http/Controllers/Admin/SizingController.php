<?php

namespace App\Http\Controllers\Admin;

use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SizingController extends Controller
{
    public function index()
    {
        $sizes = Size::get();
        return view('admin.sizes.index', compact('sizes'));
    }


    public function create()
    {
        return view('admin.sizes.single');
    }



    public function store(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Size::create($request->all());

        return redirect('/admin/sizes/index')->with('success','تم إضافة حجم جديد بنجاح');
    }


    public function edit($id)
    {
        $size = Size::find($id);
        return view('admin.sizes.single', compact('size'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:sizes,id',
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Size::find($request->id)->update($request->all());

        return redirect('/admin/sizes/index')->with('success','تم تعديل الحجم بنجاح');
    }
}
