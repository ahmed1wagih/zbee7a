<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function admins()
    {
        $users = User::where('type','admin')->where('id','!=',1)->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function users()
    {
        $users = User::where('type','user')->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function search()
    {
        $search = Input::get('search');
        $users = User::where(function($q) use($search)
        {
            $q->where('name','like','%'.$search.'%');
            $q->orWhere('email','like','%'.$search.'%');
            $q->orWhere('phone','like','%'.$search.'%');
        }
        )->paginate(50);

        return view('admin.users.index', compact('users','search'));
    }


    public function create()
    {
        return view('admin.users.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'type' => 'required|in:admin,user',
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|unique:users,phone',
                'password' => 'required|confirmed|min:6',
                'address' => 'required',
            ],
            [
                'type.required' => 'نوع المستخدم مطلوب',
                'type.in' => 'نوع المستخدم غير صحيح',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف مطلوب',
                'phone.unique' => 'الهاتف موجود من قبل',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
                'address.required' => 'العنوان مطلوب',
            ]
        );


        $user = new User();
            $user->type = $request->type;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = md5($request->password);
            $user->address = $request->address;
        $user->save();

        return redirect('/admin/'.$user->type.'s/index')->with('success','تمت الإضافة بنجاح');
    }


    public function edit($id, Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'required|exists:users,id|not_in:1',
            ]
        );

        $user = User::find($id);
        return view('admin.users.single', compact('user'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id|not_in:1',
                'type' => 'required|in:admin,user',
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->user_id,
                'phone' => 'required|unique:users,phone,'.$request->user_id,
                'password' => 'sometimes|nullable|confirmed|min:6',
                'address' => 'required',
            ],
            [
                'type.required' => 'نوع المستخدم مطلوب',
                'type.in' => 'نوع المستخدم غير صحيح',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف مطلوب',
                'phone.unique' => 'الهاتف موجود من قبل',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
                'address.required' => 'العنوان مطلوب',
            ]
        );

        $user = User::find($request->user_id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            if($request->password) $user->password = md5($request->password);
            $user->address = $request->address;
        $user->save();

        return redirect('/admin/'.$user->type.'s/index')->with('success','تم التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id',
            ]
        );


        User::where('id', $request->user_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
