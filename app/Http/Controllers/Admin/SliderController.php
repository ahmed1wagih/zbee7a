<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::paginate(20);
        return view('admin.sliders.index', compact('sliders'));
    }

    public function create()
    {
        return view('admin.sliders.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'link' => 'required',
                'file' => 'required|image|max:4096',
                'expire_at' => 'required|date'
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'file.required' => 'الصورة مطلوبة',
                'file.image' => 'صيغة الصورة غير صحيحة',
                'file.max' => 'حجم الصورة أكبر من 4 ميجا',
                'expire_at.required' => 'تاريخ الإنتهاء مطلوب',
                'expire_at.date' => 'صيغة تاريخ الإنتهاء غير صحيحة',
            ]
        );
        $slider = new Slider();
        $slider->link = $request->link;

        $photo = unique_file($request->file->getClientOriginalName());
        $request->file->move(base_path() .'/public/sliders/',$photo);
        $slider->image = $photo;

        $slider->expire_at = $request->expire_at;
        $slider->save();

        return redirect('/admin/settings/sliders/index')->with('success', 'تمت إضافة الصورة بنجاح');
    }


    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.single', compact('slider'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'slider_id' => 'required|exists:sliders,id',
                'link' => 'required',
                'file' => 'sometimes|image|max:4096',
                'expire_at' => 'required|date'
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'file.image' => 'صيغة الصورة غير صحيحة',
                'file.max' => 'حجم الصورة أكبر من 4 ميجا',
                'expire_at.required' => 'تاريخ الإنتهاء مطلوب',
                'expire_at.date' => 'صيغة تاريخ الإنتهاء غير صحيحة',
            ]
        );
        $slider = Slider::find($request->slider_id);
        $slider->link = $request->link;

        if($request->file)
        {
//            unlink(base_path() .'/public/sliders/'.$slider->image);

            $photo = unique_file($request->file->getClientOriginalName());
            $request->file->move(base_path() . '/public/sliders/', $photo);
            $slider->image = $photo;
        }
        $slider->expire_at = $request->expire_at;
        $slider->save();

        return redirect('/admin/settings/sliders/index')->with('success', 'تمت التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'slider_id' => 'required|exists:sliders,id'
            ]
        );

        $slider = Slider::find($request->slider_id);

        unlink(base_path() .'/public/sliders/'.$slider->image);

        $slider->delete();

        return back()->with('success', 'تم الحذف بنجاح');

    }

}
