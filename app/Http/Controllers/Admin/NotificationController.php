<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class NotificationController extends Controller
{
    public function index()
    {
        $nots = Notification::latest()->paginate(50);
        return view('admin.notifications.index', compact('nots'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'text' => 'required'
            ],
            [
                'text.required' => 'عفواً,النص مطلوب'
            ]
        );

        Notification::create(
            [
                'text' => $request->text
            ]
        );

        Notification::send($request->text);

        return back()->with('success', 'تم إرسال الإشعار بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'not_id' => 'required|exists:notifications,id'
            ]
        );

        Notification::where('id', $request->not_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
