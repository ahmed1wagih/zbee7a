<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cutting;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\Packaging;
use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use mysql_xdevapi\Exception;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::paginate(50);
        return view('admin.items.index', compact('items'));
    }


    public function search()
    {
        $search = Input::get('search');
        $items = Item::where(function($q) use($search)
        {
            $q->where('title_ar','like','%'.$search.'%');
            $q->orWhere('title_en','like','%'.$search.'%');
        }
        )->paginate(50);

        return view('admin.items.index', compact('items','search'));
    }


    public function create()
    {
        $cuttings = Cutting::all();
        $packagings = Packaging::all();
        $sizes = Size::all();

        return view('admin.items.single', compact('cuttings','packagings','sizes'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title_ar' => 'required',
                'title_en' => 'required',
                'details_ar' => 'required',
                'details_en' => 'required',
                'price_before' => 'required|integer',
                'price_after' => 'required|integer',
                'cutting_id' => 'required|exists:cuttings,id',
                'packaging_id' => 'required|exists:packagings,id',
                'size_id' => 'required|exists:sizes,id',
                'images' => 'required|array',
                'images.*' => 'required|image'
            ],
            [
                'title_ar.required' => 'العنوان بالعربية مطلوب',
                'title_en.required' => 'العنوان بالإنجليزية مطلوب',
                'details_ar.required' => 'التفاصيل بالعربية مطلوبة',
                'details_en.required' => 'التفاصيل بالإنجليزية مطلوبة',
                'price_before.required' => 'السعر قبل مطلوب',
                'price_after.required' => 'السعر بعد مطلوب',
                'cutting_id.required' => 'نوع التقطيع مطلوب',
                'cutting_id.exists' => 'عفواً,نوع التقطيع غير صحيح',
                'packaging_id.required' => 'نوع التغليف مطلوب',
                'packaging_id.exists' => 'عفواً,نوع التغليف غير صحيح',
                'size_id.required' => 'نوع الحجم مطلوب',
                'size_id.exists' => 'عفواً,نوع الحجم غير صحيح',
                'images.required' => 'الهاتف موجود من قبل',
                'images.*.image' => 'الهاتف موجود من قبل',
            ]
        );


        $item = new Item();
            $item->title_ar = $request->title_ar;
            $item->title_en = $request->title_en;
            $item->details_ar = $request->details_ar;
            $item->details_en = $request->details_en;
            $item->price_before = $request->price_before;
            $item->price_after = $request->price_after;
            $item->cutting_id = $request->cutting_id;
            $item->packaging_id = $request->packaging_id;
            $item->size_id = $request->size_id;
        $item->save();

        foreach($request->images as $image)
        {
            $name = unique_file($image->getClientOriginalName());
            $image->move(base_path().'/public/items', $name);

            $image = new ItemImage();
                $image->item_id = $item->id;
                $image->image = $name;
            $image->save();
        }

        return redirect('/admin/items/index')->with('success','تمت الإضافة بنجاح');
    }


    public function edit($id, Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'required|exists:items,id',
            ]
        );

        $item = Item::find($id);
        $cuttings = Cutting::all();
        $packagings = Packaging::all();
        $sizes = Size::all();

        return view('admin.items.single', compact('item','cuttings','packagings','sizes'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'item_id' => 'required|exists:items,id',
                'title_ar' => 'required',
                'title_en' => 'required',
                'details_ar' => 'required',
                'details_en' => 'required',
                'price_before' => 'required|integer',
                'price_after' => 'required|integer',
                'cutting_id' => 'required|exists:cuttings,id',
                'packaging_id' => 'required|exists:packagings,id',
                'size_id' => 'required|exists:sizes,id',
                'images' => 'sometimes|array',
                'images.*' => 'image',
                'deleted_ids' => 'sometimes|array'
            ],
            [
                'title_ar.required' => 'العنوان بالعربية مطلوب',
                'title_en.required' => 'العنوان بالإنجليزية مطلوب',
                'details_ar.required' => 'التفاصيل بالعربية مطلوبة',
                'details_en.required' => 'التفاصيل بالإنجليزية مطلوبة',
                'price_before.required' => 'السعر قبل مطلوب',
                'price_after.required' => 'السعر بعد مطلوب',
                'cutting_id.required' => 'نوع التقطيع مطلوب',
                'cutting_id.exists' => 'عفواً,نوع التقطيع غير صحيح',
                'packaging_id.required' => 'نوع التغليف مطلوب',
                'packaging_id.exists' => 'عفواً,نوع التغليف غير صحيح',
                'size_id.required' => 'نوع الحجم مطلوب',
                'size_id.exists' => 'عفواً,نوع الحجم غير صحيح',
                'images.*.image' => 'الصور غيرصحيحة',
            ]
        );
       
        $item = Item::find($request->item_id);
            $item->title_ar = $request->title_ar;
            $item->title_en = $request->title_en;
            $item->details_ar = $request->details_ar;
            $item->details_en = $request->details_en;
            $item->price_before = $request->price_before;
            $item->price_after = $request->price_after;
            $item->cutting_id = $request->cutting_id;
            $item->packaging_id = $request->packaging_id;
            $item->size_id = $request->size_id;
        $item->save();

        if($request->images)
        {
            foreach($request->images as $image)
            {
                $name = unique_file($image->getClientOriginalName());
                $image->move(base_path() . '/public/items', $name);

                $image = new ItemImage();
                    $image->item_id = $item->id;
                    $image->image = $name;
                $image->save();
            }
        }

        if($request->deleted_ids)
        {
            foreach($request->deleted_ids as $id)
            {
                $image = ItemImage::where('id', $id)->first();
//                unlink(base_path().'/public/items/'.$image->image);
                $image->delete();
            }
        }


        return redirect('/admin/items/index')->with('success','تم التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'item_id' => 'required|exists:items,id',
            ]
        );


        Item::where('id', $request->item_id)->delete();
        $images = ItemImage::where('item_id', $request->item_id)->get();

        foreach($images as $image)
        {
            try
            {
                unlink(base_path().'/public/items/'.$image->image);
                $image->delete();
            }
            catch(Exception $exception)
            {

            }

        }

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
