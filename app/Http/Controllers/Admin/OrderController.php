<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    public function index($status)
    {
        $flag = $status == 'awaiting' ? 'قيد الإنتظار' : 'منتهية';
        $orders = Order::where('status',$status)->paginate(50);

        return view('admin.orders.index', compact('flag','orders'));
    }



    public function search()
    {
        $search = Input::get('search');
        $users = User::where(function($q) use($search)
        {
            $q->where('name','like','%'.$search.'%');
            $q->orWhere('email','like','%'.$search.'%');
            $q->orWhere('phone','like','%'.$search.'%');
        }
        )->pluck('id');

        $orders = Order::whereIn('user_id', $users)->paginate(50);

        return view('admin.orders.index', compact('orders','search'));
    }


    public function user_list($id)
    {
        $orders = Order::where('user_id', $id)->paginate(50);
        return view('admin.orders.index', compact('orders'));
    }


    public function show($id, Request $request)
    {
        $request->merge(['item_id' => $id]);
        $this->validate($request,
            [
                'item_id' => 'required|exists:orders,id'
            ]
        );

        $order = Order::find($id);

        return view('admin.orders.show', compact('order'));
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'order_id' => 'required|exists:orders,id'
            ]
        );

        Order::where('id', $request->order_id)->update(['status' => 'completed']);

        return redirect('/admin/orders/awaiting/index')->with('success', 'تم إنهاء الطلب بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'item_id' => 'required|exists:orders,id'
            ]
        );

        Order::where('id', $request->order_id)->delete();

        return back()->with('success', 'تم حذف الطلب بنجاح');
    }
}
