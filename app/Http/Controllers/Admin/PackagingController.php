<?php

namespace App\Http\Controllers\Admin;

use App\Models\Packaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagingController extends Controller
{
    public function index()
    {
        $packagings = Packaging::get();
        return view('admin.packagings.index', compact('packagings'));
    }


    public function create()
    {
        return view('admin.packagings.single');
    }



    public function store(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Packaging::create($request->all());

        return redirect('/admin/packagings/index')->with('success','تم إضافة تغليف جديد بنجاح');
    }


    public function edit($id)
    {
        $packaging = Packaging::find($id);
        return view('admin.packagings.single', compact('packaging'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:packagings,id',
                'ar_name' => 'required',
                'en_name' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالعربية مطلوب'
            ]
        );

        Packaging::find($request->id)->update($request->all());

        return redirect('/admin/packagings/index')->with('success','تم تعديل التغليف بنجاح');
    }
}
