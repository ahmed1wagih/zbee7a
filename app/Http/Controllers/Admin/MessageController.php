<?php

namespace App\Http\Controllers\Admin;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::paginate(50);
        return view('admin.messages.index', compact('messages'));
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'message_id' => 'required|exists:messages,id'
            ]
        );

        Message::where('id', $request->message_id)->delete();

        return back()->with('success', 'تم حذف الرسالة بنجاح');
    }
}
