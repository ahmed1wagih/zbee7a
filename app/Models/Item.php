<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Item extends Model
{
    public function images()
    {
        return $this->hasMany(ItemImage::class, 'item_id');
    }


    public function cover()
    {
        return $this->hasOne(ItemImage::class, 'item_id');
    }


    public function cutting()
    {
        return $this->belongsTo(Cutting::class, 'cutting_id');
    }


    public function packaging()
    {
        return $this->belongsTo(Packaging::class, 'packaging_id');
    }


    public function size()
    {
        return $this->belongsTo(Size::class, 'size_id');
    }
}
