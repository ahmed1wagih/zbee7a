<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function get_items_count($order_id)
    {
        $order = Order::where('id', $order_id)->select('basket_items')->first();
        $count = explode(',',$order->basket_items);

        return count($count);
    }


    public function get_items($basket_items)
    {
        $basket_items = explode(',', $basket_items);
        $items = Item::whereIn('id',$basket_items)->get();

        return $items;
    }
}
