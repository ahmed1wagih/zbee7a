<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected function updateTimestamps()
    {
        return false;
    }

    protected $fillable =
        [
            'text'
        ];


    public static function send($text)
    {
        $tokens = User::pluck('device_token')->unique();

        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'notification' =>
                [
                    'message' => $text,
                ],
            'data' =>
                [
                    'message' => $text,
                ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            'AAAAxkGKVsU:APA91bFSqNu-kwi9oyW5bjC8vyayWAvMASAZlDQ41-v502ON57oEOUZxhcG6egVB7Xjt_BCBtmzGZHtVAR7k3AY-QJ-z69YUYf33iRzhgvbjNuHVgdjqI73BB4_39y6JLF8vGqjGfm1L'

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }
}
