<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cutting extends Model
{
    protected $fillable =
        [
            'ar_name','en_name'
        ];

    public $timestamps = false;
}
