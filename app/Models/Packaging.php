<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packaging extends Model
{
    protected $fillable =
        [
            'ar_name','en_name'
        ];

    public $timestamps = false;
}
