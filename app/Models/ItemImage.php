<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemImage extends Model
{
    protected $table = 'items_images';
    public $timestamps = false;

    protected $fillable =
        [
            'item_id','image'
        ];
}
